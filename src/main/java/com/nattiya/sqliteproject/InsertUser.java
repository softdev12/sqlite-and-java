/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nattiya.sqliteproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author DELL
 */
public class InsertUser {

    public static void main(String[] args) {
        Connection conn = null;
        String dbName = "user.db";
        Statement stmt = null;

        //Connection
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:" + dbName);
            stmt = conn.createStatement();

            String sql = "INSERT INTO USER (ID,USERNAME,PASSWORD) "
                    + "VALUES (0, 'boss','password');";
            stmt.executeUpdate(sql);
            
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException ex) {
            System.out.println("No library org.sqlite.JDBC!!!");
            System.exit(0);
        } catch (SQLException ex) {
            System.out.println("Unable to connection database!!!");
            System.exit(0);
        }
    }

}
